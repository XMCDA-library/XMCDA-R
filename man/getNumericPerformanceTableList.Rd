\name{getNumericPerformanceTableList}
\alias{getNumericPerformanceTableList}

\title{
Read numeric performance tables
}

\description{
Extract all numeric performance tables from a Java xmcda object.
}

\usage{
getNumericPerformanceTableList(xmcda)
}

\arguments{
  \item{xmcda}{
Java object containing the \code{XMCDA} XML tree.
}
}


\value{
Returns a list of matrices containing each a performance table. 
The rows of the matrices are named according to the ids of the alternatives, whereas the columns are named according to the ids of the criteria. 
Each element of the list is named according to the id of the performance table, if it is specified. Returns an empty list if no performance table is found.
}

\examples{
# create a parser object (see Javadoc)

parser<-.jnew("org/xmcda/parsers/xml/xmcda_v3/XMCDAParser")

# create an xmcda object (see Javadoc)

xmcda<-.jnew("org/xmcda/XMCDA")

# read an XMCDA file called "littleProblem.xml" 
# (via the readXMCDA method of the parser object),
# and store it in the xmcda object

parser$readXMCDA(xmcda,system.file("extdata",
		"littleProblem.xml",
		package="XMCDA3"))

pTList <- getNumericPerformanceTableList(xmcda)

}

\keyword{performance table}

