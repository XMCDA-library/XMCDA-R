\name{putProgramExecutionResult}
\alias{putProgramExecutionResult}
\title{
Put methodExecutionResults to XMCDA
}
\description{
Put methodExecutionResults to a Java xmcda object.
}
\usage{
putProgramExecutionResult(xmcda,  
                          infos = NULL, 
                          warnings = NULL, 
                          errors = NULL, 
                          debugs = NULL)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{xmcda}{
The Java object containing an XMCDA tree which should be completed by the data from the input arguments.
}
  \item{infos}{
A vector of strings containing the contents of info messages to add to the \code{methodExecutionResults}.
}
  \item{warnings}{
A vector of strings containing the contents of warning messages to add to the \code{methodExecutionResults}.
}
  \item{errors}{
A vector of strings containing the contents of error messages to add to the \code{methodExecutionResults}.
}
  \item{debugs}{
A vector of strings containing the contents of debug messages to add to the \code{methodExecutionResults}.
}
}

\value{
Returns a completed Java xmcda object.
}

\examples{

# create an xmcda object (see Javadoc)

xmcda<-.jnew("org/xmcda/XMCDA")

putProgramExecutionResult(xmcda,"OK",
          infos=c("Everything went fine"))

writeXMCDA(xmcda,"killme.xml")
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
