\name{getActiveAlternatives}
\alias{getActiveAlternatives}

\title{Get the active alternatives}

\description{Extracts the active alternatives from a Java xmcda object.}

\usage{
getActiveAlternatives(xmcda)
}

\arguments{
  \item{xmcda}{Java object containing the \code{XMCDA} XML tree.}
  }

\value{
  The function returns a list structured as follows: 
  \item{alternatives}{A list of Java \code{alternative} objects.} 
  \item{alternativesIDs}{A vector containing the \code{id}s of the active alternatives.}
  \item{numberOfAlternatives}{The number of active alternatives.}
}

\examples{
# create a parser object (see Javadoc)

parser<-.jnew("org/xmcda/parsers/xml/xmcda_v3/XMCDAParser")

# create an xmcda object (see Javadoc)

xmcda<-.jnew("org/xmcda/XMCDA")

# read an XMCDA file called "littleProblem.xml" 
# (via the readXMCDA method of the parser object),
# and store it in the xmcda object

parser$readXMCDA(xmcda,system.file("extdata",
		"littleProblem.xml",
		package="XMCDA3"))

alternatives <- getActiveAlternatives(xmcda)

}

\keyword{alternatives}

